<?php
//register.php

require_once 'includes/global.inc.php';

//initialize php variables used in the form
$username = "";
$password = "";
$password_confirm = "";
$email = "";
$location = "";
$birthday = "";
$uni = "";
$major = "";
$error = "";

//check to see that the form has been submitted
if(isset($_POST['submit-form'])) { 

	//retrieve the $_POST variables
	$username = $_POST['username'];
	$password = $_POST['password'];
	$password_confirm = $_POST['password-confirm'];
	$email = $_POST['email'];
	$birthday = $_POST['birthday'];
	$location = $_POST['location'];
	$uni = $_POST['uni'];
	$major = $_POST['major'];

	//initialize variables for form validation
	$success = true;
	$userTools = new UserTools();

	//validate that the form was filled out correctly
	//check to see if user name already exists
	if($userTools->checkUsernameExists($username))
	{
	    $error .= "That username is already taken.<br/> \n\r";
	    $success = false;
	}

	//check to see if passwords match
	if($password != $password_confirm) {
	    $error .= "Passwords do not match.<br/> \n\r";
	    $success = false;
	}

	if($success)
	{
	    //prep the data for saving in a new user object
	    $data['username'] = $username;
	    $data['password'] = md5($password); //encrypt the password for storage
	    $data['email'] = $email;
	    $data['location'] = $location;
	    $data['birthday'] = $birthday;
	    $data['uni'] = $uni;
	    $data['major'] = $major;

	    //create the new user object
	    $newUser = new User($data);

	    //save the new user to the database
	    $newUser->save(true);

	    //log them in
	    $userTools->login($email, $password);

	    //redirect them to a welcome page
	    header("Location: welcome.php");
	}
}

//If the form wasn't submitted, or didn't validate
//then we show the registration form again
?>

<html>
  
  <head>
  	<title>Sign Up</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
    rel="stylesheet" type="text/css">
  </head>
  
  <body>
    <div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><span>Group Study</span></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li>
             <a href="index.php">Login</a>
            </li>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <form role="form" class="text-left"  action="register.php" method="post">

            	<div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Name</label>
                <input class="form-control" name="username"
                placeholder="Enter Your Name" type="text" value="<?php echo $username;?>" required>
              </div>

              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Password</label>
                <input class="form-control" name="password"
                placeholder="Password" value="<?php echo $password; ?>"  type="password" required>

              <div class="form-group">
                <label class="control-label" for="exampleInputPassword1">Password (confirm)</label>
                <input class="form-control" name="password-confirm" 
                placeholder="Password" value="<?php echo $password_confirm; ?>" type="password" required>

              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Email address</label>
                <input class="form-control" 
                placeholder="Enter E-Mail" type="email" value="<?php echo $email; ?>" name="email" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Country</label>
                <input class="form-control"
                placeholder="" type="text" value="<?php echo $location; ?>" name="location" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Birthday</label>
                <input class="form-control" 
                placeholder="Birthday" type="date" value="<?php echo $birthday; ?>" name="birthday" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">University</label>
                <input class="form-control" 
                placeholder="Enter University Name" type="text" value="<?php echo $uni; ?>" name="uni" required>
              </div>
              <div class="form-group">
                <label class="control-label" for="exampleInputEmail1">Faculty</label>
                <input class="form-control" 
                placeholder="Enter Your Faculty" type="text" value="<?php echo $major; ?>" name="major" required>
              </div>
              </div>
              <button type="submit" class="btn btn-default" name="submit-form">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </body>

</html>