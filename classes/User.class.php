<?php
//User.class.php

require_once 'DB.class.php';


class User {

	public $id;
	public $username;
	public $hashedPassword;
	public $email;
	public $joinDate;
	public $location;
	public $birthday;
	public $uni;
	public $major;


	//Constructor is called whenever a new object is created.
	//Takes an associative array with the DB row as an argument.
	function __construct($data) {
		$this->id = (isset($data['id'])) ? $data['id'] : "";
		$this->username = (isset($data['username'])) ? $data['username'] : "";
		$this->hashedPassword = (isset($data['password'])) ? $data['password'] : "";
		$this->email = (isset($data['email'])) ? $data['email'] : "";
		$this->location = (isset($data['location'])) ? $data['location'] : "";
		$this->birthday = (isset($data['birthday'])) ? $data['birthday'] : "";
		$this->uni = (isset($data['uni'])) ? $data['uni'] : "";
		$this->major = (isset($data['major'])) ? $data['major'] : "";
	}

	public function save($isNewUser = false) {
		//create a new database object.
		$db = new DB();
		
		//if the user is already registered and we're
		//just updating their info.
		$a = "'".$this->location."'";
		$cont = $db->select('countries','country_name =' .$a);
		if (isset($cont['country_id']) ) {
			$this->location = $cont['country_id'];
		} else {
			$temp = array(
				"country_name"=>"'$this->location'");
			$this->location = $db->insert($temp, 'countries');
		}
		$uniArray = $db->select('universities','uni_name ='. "'".$this->uni."'" .'AND country_id = '.$this->location);
		if (isset($uniArray['uni_id'])) {
			$this->uni = $uniArray['uni_id'];
		} else {
			$temp = array(
				"uni_name"=>"'$this->uni'",
				"country_id" => "'$this->location'");
			$this->uni = $db->insert($temp, 'universities');
		}

	
		$facArray = $db->select('faculties','faculty_name ='. "'".$this->major."'" .'AND uni_id = '.$this->uni);
		if (isset($facArray['faculty_id'])) {
			$this->major = $facArray['faculty_id'];
		} else {
			$temp = array(
				"faculty_name"=>"'$this->major'",
				"uni_id" => "'$this->uni'");
			$this->major = $db->insert($temp, 'faculties');
		}

		if(!$isNewUser) {
			//set the data array
			$data = array(
				"username" => "'$this->username'",
				"password" => "'$this->hashedPassword'",
				"email" => "'$this->email'",
				"birthday" => "'$this->birthday'",
				"location" => "'$this->location'",
				"uni" => "'$this->uni'",
				"major" => "'$this->major'"
			);
			
			//update the row in the database
			$db->update($data, 'users', 'id = '.$this->id);
		}else {
		//if the user is being registered for the first time.
			$data = array(
				"username" => "'$this->username'",
				"password" => "'$this->hashedPassword'",
				"email" => "'$this->email'",
				"birthday" => "'$this->birthday'",
				"location" => "'$this->location'",
				"uni" => "'$this->uni'",
				"major" => "'$this->major'"
			);
			
			$this->id = $db->insert($data, 'users');
			$this->joinDate = time();
		}
		return true;
	}
	
}

?>