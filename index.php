<?php
//index.php 
require_once 'includes/global.inc.php';
require_once 'facebook-php-sdk-v4-5.0.0\src\Facebook\autoload.php';


$error = "";
$email = "";
$password = "";

if(isset($_SESSION['logged_in'])) {
       $user = unserialize($_SESSION['user']); 
       header("Location: welcome.php");	
	}

//check to see if they've submitted the login form
if(isset($_POST['submit-login'])) { 

	$email = $_POST['email'];
	$password = $_POST['password'];
	$userTools = new UserTools();
	if($userTools->login($email, $password)){
		//successful login, redirect them to a page
		header("Location: index.php");
	}else{
		$error = "Incorrect E-mail or password. Please try again.";
	}
}
?>



<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
    rel="stylesheet" type="text/css">
    <link href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
    rel="stylesheet" type="text/css">
</head>
<body>
<div class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-ex-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#"><span>Group Study</span></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-ex-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li >
            	<?php 
            		if($error != "")
					{
						    echo $error."<br/>";
					}
				?>
				<form action="index.php" method="post">
	    			<ul><input type="text" name="email" value="<?php echo $email;?>" placeholder="E-mail" /><br/></ul>
	    			<ul><input type="password" name="password" value="<?php echo $password; ?>" placeholder="Password"/><br/></ul>
	    			<ul> 
	    				<div class="row text-right">
	    				<input type="submit" class="btn btn-block btn-info btn-sm" value="Login" name="submit-login" />
	    				</div>
	    			</ul>
				</form>
            </li>
            <li>
            	<div class="btn btn-block">
          		<?php 
            		echo "<div class=\"fb-like\" data-share=\"true\" data-width=\"450\" data-show-faces=\"true\"></div>";
					$fb = new Facebook\Facebook(['app_id' => '559873260835651', 'app_secret' => 'cac8a821ef8e25371ac6cd759a030e87', 'default_graph_version' => 'v2.2', ]);
					$helper = $fb->getRedirectLoginHelper();
					$permissions = ['public_profile','email', 'user_likes','user_events','user_posts','user_photos','user_hometown','user_birthday','user_location']; // optional
					$loginUrl = $helper->getLoginUrl('http://localhost/login-callback.php', $permissions);
					echo "<fb:login-button scope=\"public_profile,email\" ></fb:login-button>";
					echo '<a href="' . $loginUrl . '">fb</a>';
					$_SESSION["flag"] = true;
				?>
			</div>
          	</li>
            <li>
              <a href="register.php">Sign Up</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div class="section">
      <div class="container">
        <div class="row">
          <div class="col-md-6 text-left">
            <h1 class="text-center">What is Group Study?</h1>
            <p class="text-center">We are trying to help our Education system to get better.
              By this project, announcments and materials of your courses are collected together 
              in one place. Now you can  easily  be updated with everything related to your subscribed 
              courses. </p>
          </div>
          <div class="col-md-6">
            <img src="http://www.vapld.info/images/ys/books.png" class="img-responsive">
          </div>
        </div>
      </div>
    </div>
</body>
</html>