<?php
//logout.php 

require_once 'includes/global.inc.php';
if(isset($_SESSION['logged_in'])) {
 $user = unserialize($_SESSION['user']); 
  $userTools->logout();  
  header("Location: index.php"); 
} else {
	header("Location: index.php"); 
}

?>