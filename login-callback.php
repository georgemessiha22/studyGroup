<?php
//login-callback.php

//facebook pulling info
require_once 'facebook-php-sdk-v4-5.0.0\src\Facebook\autoload.php';
require_once 'includes/global.inc.php';

$userTools = new UserTools();


//initialize php variables used in the form

$password = "1";
$uni = "";
$major = "";
$error = "";

if ($_SESSION["flag"] == true){
   $fb = new Facebook\Facebook(['app_id' => '559873260835651', 'app_secret' => 'cac8a821ef8e25371ac6cd759a030e87', 'default_graph_version' => 'v2.5', ]);
//session_start();  
$helper = $fb->getRedirectLoginHelper();

$accessToken = $helper->getAccessToken();
  
// Since all the requests will be sent on behalf of the same user,
// we'll set the default fallback access token here.
$fb->setDefaultAccessToken($accessToken);
$_SESSION["fb"] = $fb;
$_SESSION["accessToken"] = $accessToken;
try {
  // Returns a `Facebook\FacebookResponse` object
  $response = $fb->get('/me?fields=email', $accessToken);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}
$email = $response->getGraphUser()->getField("email");
if ($userTools->checkUsernameExists($response->getGraphUser()->getField("email"))) {
  $userTools->login($email, $password);
  header("Location: welcome.php");
}
$_SESSION["flag"] = false;
}

//check to see that the form has been submitted
if(isset($_POST['comp'])) {
  

  //retrieve the $_POST variables
  $uni = $_POST['uni'];
  $major = $_POST['major'];
  $fb = $_SESSION["fb"];
try {
  // Returns a `Facebook\FacebookResponse` object
  $response = $fb->get('/me?fields=id,name,email,birthday,location', $_SESSION["accessToken"]);
} catch(Facebook\Exceptions\FacebookResponseException $e) {
  echo 'Graph returned an error: ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  echo 'Facebook SDK returned an error: ' . $e->getMessage();
  exit;
}

  $user = $response->getGraphUser();

$birthday=$user->getField("birthday")->format('Y-m-d');
$username =$user->getField("name");
$id=$user->getField("id");
$email= $user->getField("email");
$loc= json_decode(
  $user->getField("location"))->{'name'};
$location = substr($loc, strpos($loc, ",") + 2);    
    //prep the data for saving in a new user object
  //$location = $user->getField("country");
  $data['username'] = $username;
  $data['password'] = md5($password); //encrypt the password for storage
  $data['email'] = $email;
  $data['location'] = $location;
  $data['birthday'] = $birthday;
    $data['uni'] = $uni;
    $data['major'] = $major;

      //create the new user object
      $newUser = new User($data);

      //save the new user to the database
      $newUser->save(true);

      //log them in
      $userTools->login($email, $password);
      
      //redirect them to a welcome page
      header("Location: welcome.php");
}


?>
<html>
<head>
	<title>Logged In</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
<pre>
	<?php echo ($error != "") ? $error : ""; ?>
  <form action="login-callback.php" method="post">
  University: <input type="text" value="<?php echo $uni; ?>" name="uni" /><br/>
  Major: <input type="text" value="<?php echo $major; ?>" name="major" /><br/>
  <input type="submit" value="Register" name="comp" />

</pre>
</body>
</html>